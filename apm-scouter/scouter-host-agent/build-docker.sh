#!/bin/bash

source ../config

docker build -t pilot/scouter-host-agent:v${SCOUTER_VERSION} .
docker tag pilot/scouter-host-agent:v${SCOUTER_VERSION}  pilot/scouter-host-agent:latest
docker push pilot/scouter-host-agent:latest 

