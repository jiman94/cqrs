#!/bin/bash

source ../config
SCOUTER_VERSION=2.15.0

docker build -t pilot/scouter-server:v${SCOUTER_VERSION} .
docker tag pilot/scouter-server:v${SCOUTER_VERSION}  pilot/scouter-server:latest

#docker push pilot/scouter-server:latest 

