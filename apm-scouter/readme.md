# 테스트 빌드 

docker run -it --name scouter-server -p 6180:6180/tcp -p 6100:6100/tcp -p 6101:6101/udp pilot/scouter-server

# 대몬으로 구동 
docker run -d -it --name scouter-server -p 6100:6100/tcp -p 6101:6101/udp pilot/scouter-server 
docker run -d -it --link scouter-server pilot/scouter-host-agent

# server 
http://localhost:6180/scouter/v1/info/server

# swagger 
http://localhost:6180/swagger/index.html


### Scouter Docker Image 
 - 스카우터 페이퍼
 > 1680
 > admin /  admin 

