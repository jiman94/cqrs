# CQRS pilot Project 

- 1. EventSourcing &amp; cqrs demo project

#### 1. Required

* Java 16 
* Redis 2.x - using default port ( 6379 )
* Mysql 8.x - using default port ( 3306 ) master/slave
* Zookeeper - using default port( 2181 )
* Kafka - using port ( 9092 )
* Git
* scouter 


```sh 
./gradlew wrapper --gradle-version=7.2 --distribution-type=bin
```

#### 2. Gradle 7 버전

```sh 
  compile -> implementataion 
  testComplie ->  testImplementation
 ```

#### 3. mysql admin 계정생성 

```sql  
create user 'admin'@'%' identified by 'admin'; 
SELECT user,authentication_string,plugin,host FROM mysql.user; 
GRANT ALL PRIVILEGES ON *.* TO 'admin'@'%'; 
GRANT GRANT OPTION ON *.* TO 'admin'@'%'; 
ALTER user 'admin'@'%' IDENTIFIED WITH mysql_native_password by 'admin';
flush privileges;
```

```sql 
ALTER USER 'root'@'%' IDENTIFIED WITH mysql_native_password BY 'password';
ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'root'; 
flush privileges;
```


```sql  
create user 'app_user'@'%' identified by '1234'; 
SELECT user,authentication_string,plugin,host FROM mysql.user; 
GRANT ALL PRIVILEGES ON *.* TO 'app_user'@'%'; 
GRANT GRANT OPTION ON *.* TO 'app_user'@'%'; 
ALTER user 'app_user'@'%' IDENTIFIED WITH mysql_native_password by '1234';
flush privileges;
```



#### 4. DB 접속 ( DBeaver)
- master
localhost:31001
app_user/1234
- slave
localhost:31002
app_user/1234



#### 5. create database & table

#### 6. hosts file 수정
```sh 
sudo vi /etc/hosts
```

#### 7. SpringBoot Application 실행 
```sh 
cd member; ../gradlew bootRun
cd order; ../gradlew bootRun
cd product; ../gradlew bootRun
```

## access the URL 

https://localhost:10001/regist

 - 회원가입 
 - 로그인
 - 상품등록 


## 솔루션설치 
```java
-javaagent:./scouter/scouter.agent.jar
-Dscouter.config=./scouter/conf/scouter.conf
```

# redis local 
```yaml 
spring:
  redis:
    # store-type: redis
    host: localhost
    port: 6379
    timeout: 300s
    prefix: tutorial
```    
# redis stg     
```yaml 
spring:
  redis:
    store-type: redis
    host: pilot-dev-redis-cache.zuuqx4.clustercfg.apn2.cache.amazonaws.com
    port: 6379
    timeout: 300s
    prefix: tutorial
    cluster:
      nodes:
        - pilot-dev-redis-cache-0001-001.zuuqx4.0001.apn2.cache.amazonaws.com:6379
        - pilot-dev-redis-cache-0001-002.zuuqx4.0001.apn2.cache.amazonaws.com:6379
        - pilot-dev-redis-cache-0002-001.zuuqx4.0001.apn2.cache.amazonaws.com:6379
        - pilot-dev-redis-cache-0002-002.zuuqx4.0001.apn2.cache.amazonaws.com:6379      
 ```          
           
# redis      
```java
RedisClusterConfigurationProperties.java
RedisConfig.java
```

# StringDeserializer
```java
import com.fasterxml.jackson.databind.deser.std.StringDeserializer
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;


Hibernate 4.x -> Hibernate 5.x 변경시 유의 사항.

@GeneratedValue(strategy = GenerationType.AUTO) 
@GeneratedValue(strategy = GenerationType.IDENTITY)
```


#### log4jdbc
```java
implementataion ('com.googlecode.log4jdbc:log4jdbc:1.2')
runtime('org.springframework.boot:spring-boot-devtools')
implementataion ('com.googlecode.log4jdbc:log4jdbc:1.2')
compileOnly('org.projectlombok:lombok')
compile('org.mariadb.jdbc:mariadb-java-client')
```
```yaml 
spring:
  devtools:
    livereload:
      enabled: true
  thymeleaf:
    cache: false
    mode: LEGACYHTML5

  datasource:
    username: test
    password: test
    driver-class-name: net.sf.log4jdbc.DriverSpy
    url: jdbc:log4jdbc:mariadb://localhost:3306/test?useSSL=false

  jpa:
    hibernate:
      ddl-auto: update
    show-sql: true
    database: mysql
    database-platform: org.hibernate.dialect.MySQL5InnoDBDialect
```
#


```sh 
➜  ~ curl -I -k --http2 https://localhost:8443/hello
HTTP/2 401 
www-authenticate: Basic realm="Realm"
x-content-type-options: nosniff
x-xss-protection: 1; mode=block
cache-control: no-cache, no-store, max-age=0, must-revalidate
pragma: no-cache
expires: 0
strict-transport-security: max-age=31536000 ; includeSubDomains
x-frame-options: DENY
content-type: application/json
date: Sun, 03 Oct 2021 06:36:05 GMT

➜  ~ curl -I -k --http2 http://localhost:8080/hello 
HTTP/1.1 401 
WWW-Authenticate: Basic realm="Realm"
X-Content-Type-Options: nosniff
X-XSS-Protection: 1; mode=block
Cache-Control: no-cache, no-store, max-age=0, must-revalidate
Pragma: no-cache
Expires: 0
X-Frame-Options: DENY
Content-Type: application/json
Transfer-Encoding: chunked
Date: Sun, 03 Oct 2021 06:36:32 GMT
```

#### Case 1 인증서 제작 
```sh
keytool -genkey -alias spring -storetype PKCS12 -keyalg RSA -keysize 2048 -keystore keystore.p12 -validity 4000 
```

```yaml 
server:
  ssl:
    key-store: classpath:keystore.p12
    key-store-password: 12345678
    key-store-type: pkcs12
    key-alias: spring
    key-password: 12345678
 ```   
    
# ----

####  Case 2 keytool을 통한 인증서 및 store 제작
```sh 
keytool -genkey -alias jmryustore -keyalg RSA -keystore jmryustore.pkcs12 -storetype pkcs12

keytool -export -alias jmryustore -keystore jmryustore.pkcs12 -rfc -file jmryura.cer
 
keytool -import -alias jmryutrust -file jmryura.cer -keystore jmryutrust.pkcs12
```


#### application.yml
```yaml 
server:
  port: 10001
  servlet:
    session:
      timeout: 30
  ssl:
    enabled: true
    key-alias: jmryustore
    key-store: ssl/jmryustore.pkcs12
    key-store-password: 'jmryu!2021'
    key-password: 'jmryu!2021'
    trust-store: ssl/jmryutrust.pkcs12
    trust-store-password: 'jmryu!2021'  
```

```yaml 
server:
  port: 8443 #SSL 사용할 포트 지정 
  ssl:
        enabled: true
        key-store: "sslcert.co.kr.jks" 또는 "sslcert.co.kr.pfx" 지정 
        key-store-type: 인증서 파일 포맷별 "JKS" 또는 "PKCS12" 지정 
        key-store-password: xxxxxxxx  # jks 또는 pfx 암호
        key-alias: xxxxx.com # (필요시) Alias 명 지정. CSR 자동 생성시에는 CN 명
        trust-store: # (필요시) "sslcert.co.kr.jks" 또는 "sslcert.co.kr.pfx" 지정. CSR 직접 생성시에는, 루트/체인 스토어 파일 지정
        trust-store-password: xxxxxxxx  # (필요시) jks 또는 pfx 암호
```        