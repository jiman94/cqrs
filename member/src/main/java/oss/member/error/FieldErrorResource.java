package oss.member.error;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

/**
 * . 3. 24..
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
public class FieldErrorResource {
	private String resource;
	private String field;
	private String code;
	private String message;
}
