1. docker-compose 구성

version: '3'
services:
  mysql_master:
    image: mysql:8.0.21
    volumes:
      - ./mysql_db/master:/mysql
      - ./test_db:/test_db
      - ./db/conf/master:/etc/mysql/conf.d
      - ./seeding.sh:/docker-entrypoint-initdb.d/seeding.sh
      - ./logs:/var/log/mysql
    environment:
      - TZ=Asia/Seoul
      - MYSQL_ROOT_PASSWORD=1234
      - MYSQL_DATABASE=test_db
      - MYSQL_USER=app_user
      - MYSQL_PASSWORD=1234
    ports:
      - "31001:3306"

  mysql_slave:
    image: mysql:8.0.21
    volumes:
      - ./mysql_db/slave:/mysql
      - ./test_db:/test_db
      - ./db/conf/slave:/etc/mysql/conf.d
      - ./seeding.sh:/docker-entrypoint-initdb.d/seeding.sh
      - ./logs:/var/log/mysql
    depends_on:
      - mysql_master
    environment:
      - TZ=Asia/Seoul
      - MYSQL_ROOT_PASSWORD=1234
      - MYSQL_DATABASE=test_db
      - MYSQL_USER=app_user
      - MYSQL_PASSWORD=1234
    ports:
      - "31002:3306"
    
2. master 의 경우 ( ./init/conf/master/my-ext.conf )

[mysqld]
log_error=/var/log/mysql/mysql_error.log
log-bin=mysql-bin
server_id=1
read_only=0
general_log=1
slow_query_log=1
long_query_time=2
collation-server=utf8mb4_unicode_ci
init-connect='SET NAMES UTF8MB4'
character-set-server=UTF8MB4
group_concat_max_len=153600
log_queries_not_using_indexes=1
sql_mode=""
binlog_format=ROW
transaction-isolation=READ-COMMITTED

[client]
default-character-set=UTF8MB4

[mysql]
default-character-set=UTF8MB4

 
2. salve 의 경우 ( ./init/conf/slave/my-ext.conf )

[mysqld]
log_error=/var/log/mysql/mysql_error.log
log-bin=mysql-bin
server_id=2
read_only=1
general_log= 1
slow_query_log= 1
long_query_time=2
replicate-do-db=test_db
collation-server=utf8mb4_unicode_ci
init-connect='SET NAMES UTF8MB4'
character-set-server=UTF8MB4
group_concat_max_len=153600
log_queries_not_using_indexes = 1
binlog_format=ROW
transaction-isolation=READ-UNCOMMITTED

[client]
default-character-set=UTF8MB4

[mysql]
default-character-set=UTF8MB4
 
# master 의 경우 ( ./mysql_db/master/mysql.sql )

use mysql;

CREATE USER 'repl'@'%' IDENTIFIED WITH 'mysql_native_password' BY '1234';

GRANT REPLICATION SLAVE ON *.* TO 'repl'@'%';

GRANT INSERT,SELECT,UPDATE,DELETE ON `test_db`.* TO `app_user`@`%`;

# slave 의 경우 ( ./mysql_db/slave/mysql.sql )

use mysql;

CHANGE MASTER TO MASTER_HOST='mysql_master', MASTER_PORT=3306, MASTER_USER='repl', MASTER_PASSWORD='1234', MASTER_LOG_FILE='mysql-bin.000003', MASTER_LOG_POS=156;

GRANT INSERT,SELECT,UPDATE,DELETE ON `test_db`.* TO `app_user`@`%`;
MASTER_LOG_FILE , MASTER_LOG_POS 의 경우. 실제 MASTER 에 접속 후, SHOW MASTER STATUS; 명령을 통하여 실제 값을 확인 후 입력한다.

# ./test_db/schema.sql 을 통하여 두 DB 에 동기화될 테이블을 생성한다.

DROP DATABASE IF EXISTS test_db;

CREATE DATABASE test_db DEFAULT CHARSET utf8mb4;

USE test_db;
SET sql_mode = '';

DROP TABLE if EXISTS LONGMAIL_INTERFACE;
CREATE TABLE `MAIL_INTERFACE` (
  `SEQ` INT NOT NULL AUTO_INCREMENT,
  `EMAIL` VARCHAR(100) DEFAULT NULL,
  `NAME` VARCHAR(100) DEFAULT NULL,
  `SERVICE_NAME` VARCHAR(45) DEFAULT NULL,
  `SENTTIME` DATETIME DEFAULT NULL,
  `SENDYN` CHAR(1) DEFAULT 'N',
  PRIMARY KEY (`SEQ`),
  KEY `idx_MAIL_INTERFACE_1` (`SEQ`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
 

# seeding.sh

각 DB에서 수행될 script 로 위에서 생성한 mysql.sql , schema.sql 을 수행하도록 한다.

#!/bin/bash

set -e #Exit immediately if a command exits with a non-zero status.

mysql_sqls=(
    "mysql.sql"
    )

test_db=(
	"schema.sql"
	)

sqlExecute() {
	path=$1
	shift
	sqls=("${@}")

	for file in "${sqls[@]}"
	do
		echo "- import: /$path/$file"
		mysql --default-character-set=utf8 -uroot -p1234 < "/$path/$file"
	done
}


sqlExecute "mysql" "${mysql_sqls[@]}"

sqlExecute "test_db" "${test_db[@]}"

# 실행 및 테스트


mysql slave 에 접속하여 SHOW SLAVE STATUS; 시 아래처럼 메시지가 나오면 정상이다.

이제 mysql master 에 접속하여 insert 를 해본다.

master 접속 후 아래  insert & select 를 하면 데이터가 확인이 된다.

*) mysql 버젼에 따라 설정이 다를수 있으므로 사전에 버젼을 선택 후, 설정을 확인하여야 한다.

CHANGE MASTER TO MASTER_HOST='172.22.0.3', MASTER_USER='root', MASTER_PASSWORD='password', MASTER_LOG_FILE='mysql-bin.000013', MASTER_LOG_POS=0, GET_MASTER_PUBLIC_KEY=1;

start slave;

show slave status\G;
