#!/bin/bash

set -e #Exit immediately if a command exits with a non-zero status.

mysql_sqls=(
    "mysql.sql"
    )

test_db=(
	"schema.sql"
	)

member_db=(
	"member.sql"
	)

product_db=(
	"product.sql"
	)

order_db=(
	"order.sql"
	)


sqlExecute() {
	path=$1
	shift
	sqls=("${@}")

	for file in "${sqls[@]}"
	do
		echo "- import: /$path/$file"
		mysql --default-character-set=UTF8MB4 -uroot -p1234 < "/$path/$file"
	done
}


sqlExecute "mysql" "${mysql_sqls[@]}"

sqlExecute "test_db" "${test_db[@]}"

sqlExecute "member_db" "${member_db[@]}"

sqlExecute "product_db" "${product_db[@]}"

sqlExecute "order_db" "${order_db[@]}"