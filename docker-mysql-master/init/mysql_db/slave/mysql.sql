use mysql;

CHANGE MASTER TO MASTER_HOST='mysql_master', MASTER_PORT=3306, MASTER_USER='repl', MASTER_PASSWORD='1234', MASTER_LOG_FILE='mysql-bin.000003', MASTER_LOG_POS=156;

GRANT INSERT,SELECT,UPDATE,DELETE ON `test_db`.* TO `app_user`@`%`;


ALTER USER 'app_user'@'%' IDENTIFIED WITH 'mysql_native_password' BY '1234';

GRANT ALL PRIVILEGES ON *.* TO 'app_user'@'%'; 
GRANT GRANT OPTION ON *.* TO 'app_user'@'%'; 
