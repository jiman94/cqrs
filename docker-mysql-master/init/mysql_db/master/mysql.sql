use mysql;

CREATE USER 'repl'@'%' IDENTIFIED WITH 'mysql_native_password' BY '1234';

GRANT REPLICATION SLAVE ON *.* TO 'repl'@'%';

GRANT INSERT,SELECT,UPDATE,DELETE ON `test_db`.* TO `app_user`@`%`;

ALTER USER 'app_user'@'%' IDENTIFIED WITH 'mysql_native_password' BY '1234';

GRANT ALL PRIVILEGES ON *.* TO 'app_user'@'%'; 
GRANT GRANT OPTION ON *.* TO 'app_user'@'%'; 
