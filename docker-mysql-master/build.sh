#!/bin/bash

docker-compose down
rm -rf ./data/master/*
rm -rf ./data/slave/*
docker-compose build
docker-compose up -d

