package oss.order.exception;

/**
 * . 4. 18..
 */
public class MemberNotSignedException extends RuntimeException {
	public MemberNotSignedException(String message) {
		super(message);
	}
}
