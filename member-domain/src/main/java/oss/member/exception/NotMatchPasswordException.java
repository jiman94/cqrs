package oss.member.exception;

/**
 * . 3. 26..
 */
public class NotMatchPasswordException extends RuntimeException {
	public NotMatchPasswordException(String message) {
		super(message);
	}
}
