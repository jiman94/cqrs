package oss.member.exception;

/**
 * . 3. 26..
 */
public class NotExistsMemberException extends RuntimeException {

	public NotExistsMemberException(String message) {
		super(message);
	}
}
