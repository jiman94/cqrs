package oss.member.exception;

/**
 * . 3. 23..
 */
public class DuplicateMemberIdException extends RuntimeException {

	public DuplicateMemberIdException(String message) {
		super(message);
	}
}
