package oss.member.exception;

/**
 * . 3. 26..
 */
public class DuplicateMemberEmailException extends RuntimeException {
	public DuplicateMemberEmailException(String message) {
		super(message);
	}
}
