package oss.member.exception;

/**
 * . 3. 26..
 */
public class NotEqualsPasswordException extends RuntimeException {
	public NotEqualsPasswordException(String message) {
		super(message);
	}
}
