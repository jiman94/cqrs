package oss.member.model.read;

import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * . 4. 15..
 */
@Getter
@NoArgsConstructor
public class MemberAuthToken {
	private String id;
	private String token;

	public MemberAuthToken(String id, String token) {
		this.id = id;
		this.token = token;
	}
}
