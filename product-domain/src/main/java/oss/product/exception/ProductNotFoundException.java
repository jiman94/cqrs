package oss.product.exception;

/**
 * . 4. 18..
 */
public class ProductNotFoundException extends RuntimeException {
	public ProductNotFoundException(String message) {
		super(message);
	}
}
