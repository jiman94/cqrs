package oss.product.exception;

/**
 * . 3. 27..
 */
public class InsufficientInventoryQuantityException extends RuntimeException {
	public InsufficientInventoryQuantityException(String message) {
		super(message);
	}
}
