package oss.product.model.event;

import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * . 3. 17..
 */
@Getter
@NoArgsConstructor
public class ProductPriceChanged extends AbstractProductEvent {
	private int price;
	private LocalDateTime updated;

	public ProductPriceChanged(Long productId, int price) {
		this.productId = productId;
		this.price = price;
		this.updated = LocalDateTime.now();
	}
}
