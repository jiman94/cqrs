package oss.core.event;

/**
 * 
 */
public interface EventPublisher<T extends RawEvent> {

	void publish(T rawEvent);
}
