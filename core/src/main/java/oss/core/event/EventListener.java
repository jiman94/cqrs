package oss.core.event;

/**
 * 
 */
public interface EventListener {

	void handle(Event event);
}
