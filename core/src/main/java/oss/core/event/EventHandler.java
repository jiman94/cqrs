package oss.core.event;

import java.util.List;

import oss.core.domain.AggregateRoot;

/**
 * 
 */
public interface EventHandler<A extends AggregateRoot, ID> {

	/**
	 * Save the aggregate
	 *
	 * @param aggregate
	 */
	void save(A aggregate);

	/**
	 * Get the aggregate
	 *
	 * @param identifier
	 * @return
	 */
	A find(ID identifier);

	/**
	 * Get the All aggregate
	 * @return
	 */
	List<A> findAll();
}
