package oss.core.event;

/**
 * 
 */
public interface EventProjector {
	void handle(Event event);
}
