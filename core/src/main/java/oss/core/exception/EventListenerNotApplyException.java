package oss.core.exception;

/**
 * 
 */
public class EventListenerNotApplyException extends RuntimeException {
	public EventListenerNotApplyException(String message, Exception e) {
		super(message, e);
	}
}
